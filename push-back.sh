# extract the ssh/git URL from the read-only HTTPS URL
REPOSITORY_URL=$(echo $CI_REPOSITORY_URL | perl -pe 's#.*@(.+?(\:\d+)?)/#git@\1:#')
COMMIT_MESSAGE='ci: Bump versions'
LAST_MESSAGE=$(git log -1 --pretty=%B)
export GIT_SSH=./local_ssh

if [ "$LAST_MESSAGE" == "$COMMIT_MESSAGE" ]; then 
	echo "Last commit message is '$LAST_MESSAGE', terminating to avoid loop."
	exit
fi

git config --global user.name $GITLAB_USER_NAME
git config --global user.email $GITLAB_USER_EMAIL

# add new and changed files
git add .
git commit -m "$COMMIT_MESSAGE"

# save private key from CI secret variables for pushing to repo
echo "$ssh_private_key" > ./secret_key
chmod 600 ./secret_key

# push changes back to remote branch
git push $REPOSITORY_URL HEAD:${CI_BUILD_REF_NAME}

rm ./secret_key
